import type { Participant } from '@zoomus/websdk/embedded'
import { defineStore, storeToRefs } from 'pinia'
import axios from 'axios'
import _ from 'lodash'
// import ZoomMtg from '@zoomus/websdk/'
import { useCallStore } from './call'
import { useZoomStore } from './zoom'
import { useUIStore } from './UI'
import { shareScreen, zoomUiModify } from '~/composables/zoomUI'
import { removeCall, startCall } from '~/lib/appSync'
import { roleToUrl, useAccountStore } from '~/store/account'

export const useZoomClientStore = defineStore('zoomClient', {
  state: () => ({
    client: {} as any,
    signature: '',
    clientWindow: false,
    position: { x: 0, y: 0, w: 0, h: 0, scale: 0 },
    loading: false,
    participantsList: {} as Participant[],
    uiChanging: false,
    modalMessage: '',
    // Ui state
  }),
  getters: {},
  actions: {
    async initClient() {
      ZoomMtg.setZoomJSLib('https://source.zoom.us/2.8.0/lib', '/av')
      ZoomMtg.preLoadWasm()
      ZoomMtg.prepareWebSDK()
    },
    async leaveClient() {
      ZoomMtg.leaveMeeting({
        success: async () => {
        },
      })
    },
    async endClient() {
      const zoom = useZoomStore()
      const call = useCallStore()
      ZoomMtg.endMeeting({
        success: async () => {
          zoom.model = true
          this.clientWindow = false
          removeCall(call.currentIndex)
        },
      })
    },
    join(meetingNumber = '', meetingPassword = '', callback?: Function) {
      const account = useAccountStore()
      const ui = useUIStore()
      const call = useCallStore()
      this.generateSignature(meetingNumber)
      const { sdkKey } = getMeetingOption(Number(call.currentIndex), ['sdkKey'])

      ZoomMtg.init({
        leaveUrl: roleToUrl(account.data.position),
        disablePreview: true,
        success: (_success: any) => {
          ZoomMtg.i18n.load('jp-JP')
          ZoomMtg.i18n.reload('jp-JP')
          this.loading = true
          ZoomMtg.join({
            meetingNumber,
            userName: account.data.name,
            signature: this.signature,
            sdkKey,
            passWord: meetingPassword,
            success: async () => {
              await zoomUiModify()
              this.loading = false
              await shareScreen()
              if (callback)
                await callback()
              const overlayTrigger = document.querySelector('.video-share-layout')
              if (overlayTrigger)
                overlayTrigger.addEventListener('click', () => ui.toggleOverlay())
            },
            error: (i: any) => {
              console.error(i)
            },
          })

          setInterval(() => ZoomMtg.getAttendeeslist({
            success: (i: any) => this.participantsList = i.result.attendeesList,
          }), 1000)
          // ZoomMtg.join() will go here
        },
        error: (i: any) => { console.error(i) },
      })
    },
    joinClient(id?: string, passWord?: string, callback?: Function) {
      const call = useCallStore()
      const meetingNumber = id || call.currentCall?.call.number
      const meetingPassword = passWord || call.currentCall?.call.password
      const expiration = call.currentCall?.call?.expiration || ''
      if ((new Date(expiration) > new Date()) && _.isUndefined(id) && _.isUndefined(passWord)) {
        const ui = useUIStore()
        ui.waitingRoom = true
        return
      }
      this.join(meetingNumber, meetingPassword, callback)
    },
    async startMeeting() {
      const call = useCallStore()
      const ui = useUIStore()
      const meetingNumber = call.currentCall?.call.number
      const meetingPassword = call.currentCall?.call.password
      ui.waitingRoom = false

      this.joinClient(meetingNumber, meetingPassword, async () => await startCall(call.currentCallId))
    },
    async startClient(title = '', startDate = '') {
      const account = useAccountStore()
      const zoom = useZoomStore()
      const call = useCallStore()
      const callIndex = call.availableIndex[0].id
      call.currentIndex = callIndex

      zoom.$patch({ loading: true })

      const data = await axios(`/api/create?callIndex=${callIndex}&title=${title}&startDate=${startDate}`)
        .then((res) => {
          return res.data
        })

      const {
        id,
        password,
      } = data

      await call.createCall(id, password, account.id, callIndex, startDate).then(() => {
        if (!startDate)
          this.joinClient(id, password)
      })
      zoom.loading = false
    },
    async generateSignature(meetingNumber?: string) {
      const call = useCallStore()
      const account = useAccountStore()
      const meetingNo = meetingNumber || call.currentCall?.call.number
      const { sdkKey, sdkSecret } = getMeetingOption(Number(call.currentIndex), ['sdkKey', 'sdkSecret'])

      if (sdkKey && sdkSecret) {
        this.signature = ZoomMtg.generateSDKSignature(
          {
            sdkKey,
            sdkSecret,
            meetingNumber: meetingNo,
            role: account.joinCode.toString(),
          },
        )
      }
    },
  },
})
