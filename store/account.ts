import { defineStore, storeToRefs } from 'pinia'
import _ from 'lodash'
import { useCallStore } from '~/store/call'
import type { DataCall, Role } from '~/types'
import { changeName, createUser, getUser } from '~/lib/appSync'

export function roleToUrl(role: string) {
  return `/${_.chain(/([A-Z,a-z]+)(\d*)/.exec(role))
  .splice(1, 2)
  .map(_.lowerCase)
  .join('/')
  .value()}`
}

export const useAccountStore = defineStore('account', {
  state: () => ({
    data: {
      position: '' as Role | '',
      name: '' as string | '',
    },
    id: '',
  }),
  getters: {
    joinCode: state => (['Company0', 'Company1'].includes(state.data.position) ? 1 : 0),
    full(): boolean {
      return this.joinCode === 1
    },
    ableToEnd(state) {
      const call = useCallStore()
      const creator = call.currentCall?.call?.createdBy.position
      return creator === state.data.position
    },
    availableMeeting(state): DataCall[] {
      const call = useCallStore()
      const { onGoingMeeting } = storeToRefs(call)
      if (state.data.position === 'Student')
        return onGoingMeeting.value
      return onGoingMeeting.value.filter(data => data.call.createdBy.position === state.data.position)
    },
  },
  actions: {
    initData() {
    },
    createUser(name: string, position: Role) {
      createUser(name, position).then((data) => {
        this.id = data.id
        this.data.name = name
        this.data.position = position
      })
    },
    changeName(name: string) {
      changeName(name, this.id)
      this.data.name = name
    },
  },
})
