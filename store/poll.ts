import { timingSafeEqual } from 'crypto'
import { defineStore, storeToRefs } from 'pinia'
import _ from 'lodash'
import { t } from 'chart.js/dist/chunks/helpers.core'
import { useCallStore } from './call'
import { createAns, deletePoll, getListPoll, newPoll, onCreateAnswer, onCreatePoll, onUpdateAnwer, onUpdatePoll, releasedPoll, stopPoll, votePoll } from '~/lib/appSync'

export interface Answer {
  answer: string
  id: string
  noOfAnswer: number
}

export interface PollType {
  Answer: Answer[]
  id: string
  question: string
  released: boolean
  stop: boolean
  timeStamp: string
}

interface State {
  data: PollType[]
  pollInput: boolean
  inputQuestion: string
  answeredQues: string[]
  inputAnswer: string[]
  initialQuestion: string[]
  inputId: string
}

export const usePollStore = defineStore('poll', {
  state: () =>
    ({
      data: [],
      answeredQues: [],
      pollInput: false,
      inputQuestion: '',
      inputAnswer: ['', ''],
      inputId: '',
      initialQuestion: [],
    } as State),
  getters: {
    availablePoll: state => _.filter(state.data, p => p.released && !p.stop && !_.includes(state.answeredQues, p.id)),
    modalPoll(): PollType[] {
      return _.filter(this.availablePoll, i => !this.initialQuestion.includes(i.id))
    },
  },
  actions: {
    async InitData() {
      const call = useCallStore()
      const { currentCallId } = storeToRefs(call)
      onCreateAnswer().subscribe((i) => {
        const index = _.findIndex(this.data, d => d.id === i.data.onCreatedAnswer.pollId)
        if (index > -1)
          this.data[index].Answer.push(i.data.onCreatedAnswer)
      })
      onUpdatePoll().subscribe((i) => {
        const data = i.data.onUpdatedPoll
        if (_.isNull(data.callId)) {
          this.data = _.filter(this.data, d => d.id !== data.id)
          return
        }
        console.log('The data is : ', this.data)
        console.log('The update data is : ', i)
        const index = _.findIndex(this.data, d => d.id === data.id)
        if (index >= 0)
          this.data[index] = data
      })
      onUpdateAnwer().subscribe((i) => {
        const data = i.data.onUpdatedAnswer
        this.data = _.filter(this.data, (poll) => {
          const index = _.findIndex(poll.Answer, answer => answer.id === data.id)
          if (index < 0)
            return poll
          poll.Answer[index] = data
          return poll
        })
      })
      watch(() => currentCallId.value,
        async (value) => {
          this.data = await getListPoll(currentCallId.value)
          this.initialQuestion = _.map(_.filter(this.data, p => p.released && !p.stop), i => i.id)
          onCreatePoll(value).subscribe((i) => {
            if (i.data.onCreatedPoll.callId === currentCallId.value)
              this.data.push(i.data.onCreatedPoll)
          })
        })
    },
    async newPoll(question: string, answers: string[]) {
      const call = useCallStore()
      newPoll(question, call.currentCallId).then((d) => {
        const id = d.id
        answers.forEach(a => createAns(a, id))
      })
    },
    async editPoll(question: string, answers: string[], id: string) {
      this.deletePoll(id)
      this.newPoll(question, answers)
    },
    async startEditPoll(id: string) {
      const questionObj = _.find(this.data, d => d.id === id)
      this.inputQuestion = questionObj?.question || ''
      this.inputAnswer = _.map(questionObj?.Answer, a => a.answer) || ['', '']
      this.inputId = id
      this.pollInput = true
    },
    async releasePoll(id: string) {
      releasedPoll(id)
    },
    async stopPoll(id: string) {
      stopPoll(id)
    },
    async votePoll(id: string) {
      votePoll(id)
    },
    async deletePoll(id: string) {
      deletePoll(id)
    },
    // async vote(questionId: string, answer: string) {},
  },
})
