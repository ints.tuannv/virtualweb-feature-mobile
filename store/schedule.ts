import { defineStore } from 'pinia'
import { useZoomClientStore } from './zoomClient'

function dateIsValid(date: unknown) {
  return date instanceof Date && !isNaN(date as any)
}

const stringToIso = (arg: string): string => {
  const date = new Date(arg)
  if (dateIsValid(date))
    return date.toISOString()
  return ''
}

export const useScheduleStore = defineStore('schedule', () => {
  const start = reactive({ date: '', time: '' })
  const end = reactive({ date: '', time: '' })
  const title = ref('')
  const startString = computed(() => stringToIso(`${start.date} ${start.time}`))
  const endString = computed(() => stringToIso(`${end.date} ${end.time}`))
  const zoomClient = useZoomClientStore()
  const submit = () => {
    zoomClient.startClient(title.value, startString.value)
  }

  return {
    submit,
    title,
    start,
    end,
    startString,
    endString,
  }
})
