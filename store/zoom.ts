import { defineStore } from 'pinia'
import type { EmbeddedClient, Participant } from '@zoomus/websdk/embedded'
import { initZoom, startZoomCall } from '~/lib/zoomMeeting'
import { useAccountStore } from '~/store/account'
import type { AccountRole } from '~/composables/constantAPI'
import { usePollStore } from '~/store/poll'

export const useZoomStore = defineStore('zoom', {
  state: () => ({
    client: {} as typeof EmbeddedClient,
    // Ui state
    model: true,
    loading: false,
    position: {
      x: 0,
      y: 0,
      w: 0,
      h: 0,
    },
    chat: {
      x: 0,
      y: 0,
      w: 0,
      h: 0,
    },
    className: '',
    zoomAttendees: [] as Participant[],
    menuObj: [] as string[][],
  }),
  getters: {
    noOfPartipant: state => state.zoomAttendees.length,
  },
  actions: {
    async initClient() {
      this.client = await initZoom()
    },
    // Join a meeting
    async joinClient() {
      const call = useCallStore()
      const account = useAccountStore()

      const zoom = useZoomStore()
      zoom.$patch({
        model: false,
        loading: true,
      })

      await startZoomCall(
        this.client,
        call.currentCall.meetingNumber,
        account.joinCode,
        call.currentCall.passWord,
        call.currentIndex,
        account.uuid,
      )
        .then(() => zoom.$patch({ loading: false }))
    },
    async leaveClient() {
      const zoom = useZoomStore()
      await this.client.leaveMeeting().then(() => zoom.$patch({ model: true }))
    },
    async endClient() {
      const account = useAccountStore()
      const call = useCallStore()
      const question = useQuestionStore()
      const chat = useChatStore()
      const Qa = useQAStore()
      const poll = usePollStore()
      const studentList = this.client
        .getAttendeeslist()
        .filter((att: { displayName: string }) => att.displayName !== account.uuid)
      studentList.forEach((stu: any) => this.client.expel(stu.userId))
      await this.leaveClient()
      await call.setCallData(
        {
          meetingNumber: '',
          passWord: '',
          expiration: 0,
          createBy: '',
          recordTimeStamp: 0,
        },
        call.currentIndex,
      )
      await question.Reset()
      await chat.reset()
      await Qa.reset()
      await poll.reset()
    },
    async startClient() {
      const account = useAccountStore()
      const zoom = useZoomStore()
      const callIndex = account.availableIndex[0]

      zoom.$patch({ loading: true })
      const call = useCallStore()
      fetch(`https://zoomhellogo.herokuapp.com/ping?callIndex=${callIndex}`)
        .then((response) => {
          return response.text()
        })
        .then(async (data) => {
          const {
            id,
            password,
          } = JSON.parse(data).data
          call.$patch({ currentIndex: callIndex })
          await startZoomCall(this.client, id, account.joinCode, password, callIndex, account.uuid).then(() => {
            call.setCallData(
              {
                meetingNumber: id,
                passWord: password,
                expiration: Date.now() + 40 * 60 * 1000,
                createBy: account.account as AccountRole,
                companyId: account.companyId as number,
                recordTimeStamp: 0,
              },
              callIndex,
            )
          })
        })
    },
  },
})
