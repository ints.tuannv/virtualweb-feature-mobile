import { defineStore } from 'pinia'
import _ from 'lodash'
import type { DataCall } from '~/types'
import { createCall, getListCalls, onCreateorDeleteCall, onUpdateCall, toggleRecord } from '~/lib/appSync'

export const useCallStore = defineStore('call', {
  state: () => ({
    data: [] as DataCall[],
    currentIndex: '',
    meetingStart: 0,
  }),
  getters: {
    loadingState: state => state?.data.length === 0,
    availableIndex: state => _.filter(state.data, d => _.isNull(d.call)),
    currentCall: state => _.find(state.data, d => d.id === state.currentIndex),
    currentCallId(): string {
      return String(this.currentCall?.call?.id)
    },
    currentBan(): boolean | undefined {
      return this.currentCall?.call?.commentBan
    },
    onGoingMeeting: state => _.filter(state.data, d => !_.isNull(d.call)),
  },
  actions: {
    async InitData() {
      this.data = await getListCalls()
      onCreateorDeleteCall().subscribe((i) => {
        const data = i?.data.onMutatedZoomAccount
        this.data[data.id] = i?.data.onMutatedZoomAccount
      })
      onUpdateCall().subscribe((i) => {
        const data = i?.data.onUpdatedCall
        const index = _.findIndex(this.data, value => value.call.id === data.id)
        if (index !== -1)
          this.data[Number(index)].call = data
      })
    },
    async createCall(number: string, passWord: string, userId: string, accountId: string, date = (new Date()).toString()) {
      return createCall(number, passWord, userId, accountId, date)
    },
    async toggleRecord(record: boolean) {
      toggleRecord(record, this.currentCallId)
    },
  },
})
