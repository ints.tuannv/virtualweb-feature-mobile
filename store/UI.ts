import { defineStore, storeToRefs } from 'pinia'
import { useAccountStore } from './account'

export const useUIStore = defineStore('UI', () => {
  const width = ref(0)
  const height = ref(0)
  const mTabFull = ref(false)
  const fullView = ref(false)
  const waitingRoom = ref(false)
  const sideBar = ref(false)
  const isMobile = computed(() => width.value < 640 || height.value < 500)
  const landscape = computed(() => width.value > height.value)
  const selectedTitle = ref('')
  const [overlay, toggleOverlay] = useToggle(true)
  const designNumber = computed(() => {
    const account = useAccountStore()
    const { full: company } = storeToRefs(account)
    if (!isMobile.value) {
      if (fullView.value)
        return 3
      if (company.value)
        return 2
      return 1
    }
    if (landscape.value)
      return 7
    if (fullView.value)
      return 6
    if (mTabFull.value)
      return 5
    return 4
  })

  const iconShow = computed(() => {
    return ![5, 6].includes(designNumber.value)
  })

  const isDesign7 = computed(() => {
    return designNumber.value === 7
  })

  return {
    overlay,
    toggleOverlay,
    sideBar,
    width,
    height,
    designNumber,
    waitingRoom,
    iconShow,
    fullView,
    isMobile,
    mTabFull,
    selectedTitle,
    isDesign7,
  }
})
