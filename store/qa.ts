import { defineStore, storeToRefs } from 'pinia'
import _ from 'lodash'
import { map, merge } from 'rxjs'
import { da } from 'date-fns/locale'
import { useAccountStore } from './account'
import { useCallStore } from './call'
import type { RemoveEvent } from '~/types'
import { addQA, answerQA, getListQa, onCreateQA, onUpdateQA, tickQA } from '~/lib/appSync'

export interface QAType {
  id: string
  question: string
  tick: boolean
  answer: string
  timeStamp: string
  user: {
    name: string
    id: string
  }
  answerer: {
    name: string
    id: string
  }
}

interface State {
  data: QAType[]
  subFunc: RemoveEvent
}

export const useQAStore = defineStore('QA', {
  state: () =>
    ({
      data: {},
    } as State),
  getters: {
    availableQA(): QAType[] {
      const account = useAccountStore()
      if (account.joinCode === 1)
        return this.data
      else
        return _.filter(this.data, ques => ques.user.id === account.id)
    },
  },
  actions: {
    async InitData() {
      const call = useCallStore()
      const { currentCallId } = storeToRefs(call)

      onCreateQA(currentCallId.value).subscribe((i) => {
        const data = i?.data.onCreateQA
        this.data.push(data)
      })

      watch(() => currentCallId.value,
        async (value) => {
          this.data = await getListQa(value)
          onUpdateQA(currentCallId.value).subscribe((i) => {
            const data = i?.data.onMutatedQA
            const index = _.findIndex(this.data, value => value.id === data.id)
            if (index !== -1)
              this.data[Number(index)] = data
            else
              this.data.push(data)
          })
        },
      )
    },
    async sendQuestion(question: string) {
      const call = useCallStore()
      const account = useAccountStore()
      addQA(question, account.id, call.currentCallId)
    },
    async answerQuestion(answer: string, id: string) {
      const account = useAccountStore()
      answerQA(answer, id, account.id)
    },
    async tickQuestion(id: string) {
      const account = useAccountStore()
      tickQA(id, account.id)
    },
  },
})
