import { defineStore, storeToRefs } from 'pinia'
import _ from 'lodash'
import type { Observable } from 'rxjs'

import { map, merge, reduce, tap } from 'rxjs'
import { onCreateComment, onDeleteComment } from '../lib/appSync/index'
import { useCallStore } from './call'
import { useAccountStore } from './account'
import type { RemoveEvent } from '~/types'
import { addChat, getListComments, pinChat, removeChat, toggleBan } from '~/lib/appSync'

export interface ChatMessage {
  message: string
  timeStamp: string
  sender: string
}

interface ChatObj {
  id: string
  message: string
  timeStamp: string
  user: {
    id: string
    name: string
  }
}
interface State {
  data: ChatObj[]
  subFunc: RemoveEvent
}
export interface SubDataChat {
  data: {
    onDeletedComment: { id: string }
  } | {
    onCreatedComment: {
      id: string
      message: string
      timeStamp: string
      user: {
        id: string
        name: string
      }
    }
  }
}

export const useChatStore = defineStore('chat', {
  state: () =>
    ({
      data: [],
      subFunc: () => {
      },
    } as State),
  getters: {
    pinnedChat(): ChatObj | undefined {
      const call = useCallStore()
      return _.find(this.data, i => i.id === call.currentCall?.call.pinnedCommentId)
    },
  },
  actions: {
    async InitData() {
      const call = useCallStore()
      const { currentCallId } = storeToRefs(call)
      watch(() => currentCallId.value,
        async (value) => {
          this.data = await getListComments(value)
          merge(
            onCreateComment(currentCallId.value) as Observable<SubDataChat>,
            onDeleteComment(currentCallId.value) as Observable<SubDataChat>,
          ).pipe(
            map(i => i.data),
          ).subscribe((i) => {
            if ('onDeletedComment' in i) {
              this.data = this.data.filter(message => message.id !== i.onDeletedComment.id)
            }
            else {
              if (_.find(this.data, message => message.id === i.onCreatedComment.id))
                this.data = _.map(this.data, message => message.id === i.onCreatedComment.id ? i.onCreatedComment : message)
              else
                this.data = _.concat(this.data, i.onCreatedComment)
            }
          })
        },
      )
    },
    async chat(message: string) {
      const call = useCallStore()
      const account = useAccountStore()
      const chatMessage: ChatMessage = {
        message,
        timeStamp: (new Date()).toISOString(),
        sender: account.id,
      }
      addChat(chatMessage, call.currentCall?.call.id as string)
    },
    async removeChat(id: string) {
      await removeChat(id)
    },
    async pinChat(id: string) {
      const call = useCallStore()
      await pinChat(id, call.currentCallId)
    },
    async toggleBan(value: boolean) {
      const call = useCallStore()
      await toggleBan(value, call.currentCallId)
    },
  },
})
