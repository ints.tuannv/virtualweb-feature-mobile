export default defineNitroPlugin((nitroApp) => {
  nitroApp.hooks.hook('render:html', (html) => {
    html.bodyPrepend.push(`
    <script src="https://source.zoom.us/2.8.0/lib/vendor/react.min.js"></script>
    <script src="https://source.zoom.us/2.8.0/lib/vendor/react-dom.min.js"></script>
    <script src="https://source.zoom.us/2.8.0/lib/vendor/redux.min.js"></script>
    <script src="https://source.zoom.us/2.8.0/lib/vendor/redux-thunk.min.js"></script>
    <script src="https://source.zoom.us/2.8.0/lib/vendor/lodash.min.js"></script>

    <!--&lt;!&ndash; For client view &ndash;&gt;-->
    <script src="https://source.zoom.us/zoom-meeting-2.8.0.min.js"></script><div id="zmmtg-root"><div class="root-inner"></div></div><div id="aria-notify-area" role="alert" aria-atomic="true" aria-live="assertive"></div>
    `)
  })
})
