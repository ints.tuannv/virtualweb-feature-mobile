import axios from 'axios'
import { getMeetingOption } from '~~/composables/constantAPI'

export default defineEventHandler(async (event) => {
  const query = getQuery(event)
  const { callIndex } = query

  const {
    jwtToken,
    uid,
  } = getMeetingOption(Number(callIndex), ['jwtToken', 'uid'])

  const header = {
    'content-type': 'application/json',
    'Authorization': `Bearer ${jwtToken}`,
  }

  // try {
  const idData = await axios({
    method: 'get',
    url: `https://api.zoom.us/v2/users/${uid}/meetings?type=upcoming`,
    headers: header,
  })
  return {
    data: idData.data,
  }
})
