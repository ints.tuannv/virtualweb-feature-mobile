import axios from 'axios'
import { v4 as uuidv4 } from 'uuid'
import { getMeetingOption } from '~~/composables/constantAPI'

export default defineEventHandler(async (event) => {
  const query = getQuery(event)
  const { callIndex, title, startDate } = query

  const {
    jwtToken,
    uid,
  } = getMeetingOption(Number(callIndex), ['jwtToken', 'uid'])

  const header = {
    'content-type': 'application/json',
    'Authorization': `Bearer ${jwtToken}`,
  }
  let start
  if (startDate)
    start = (`${startDate.slice(0, startDate.length - 5)}Z`).replace('.', ':')

  const body = {
    topic: title || 'Zoom meeting',
    type: 2,
    agenda: 'test',
    password: uuidv4().split('-')[0],
    // start_time: startDate || (new Date(Date.now()).toISOString()),
    start_time: start,
    recurrence: {
      type: 1,
      repeat_interval: 1,
    },
    settings: {
      host_video: 'true',
      participant_video: 'False',
      join_before_host: 'False',
      mute_upon_entry: 'true',
      watermark: 'true',
      audio: 'voip',
      auto_recording: 'none',
    },
  }

  // try {
  const idData = await axios({
    method: 'get',
    url: `https://api.zoom.us/v2/users/${uid}/meetings?type=live`,
    headers: header,
    data: body,
  })
  const id = idData.data.meetings?.[0]?.id
  if (id) {
    await axios({
      method: 'put',
      url: `https://api.zoom.us/v2/meetings/${id}/status`,
      headers: header,
      data: { action: 'end' },
    })
  }

  const response = await axios({
    method: 'post',
    url: `https://api.zoom.us/v2/users/${uid}/meetings`,
    headers: header,
    data: body,
  })
  return {
    id: response.data.id,
    password: response.data.password,
  }
  // }
  // catch (e) {
  //   console.log('Error is : ', e)
  // }
})
