import { head } from 'lodash'

export default defineNuxtConfig({
  devServer: {
    port: 4000,
  },
  modules: [
    '@vueuse/nuxt',
    '@unocss/nuxt',
    '@pinia/nuxt',
    '@nuxtjs/color-mode',
  ],
  experimental: {
    reactivityTransform: true,
    inlineSSRStyles: false,
  },
  // css: [
  //   '@unocss/reset/tailwind.css',
  // ],
  colorMode: {
    classSuffix: '',
  },
  ssr: false,
  nitro: {
    plugins: [
      '~/server/plugins/render.ts',
    ],
  },
  runtimeConfig: {
    // Private keys are only available on the server
    apiSecret: process.env.NUXT_API_SECRET || '/api',
    public: {
      zoomSdkKey: process.env.NUXT_PUBLIC_ZOOM_SDK_KEY,
      zoomSdkSecret: process.env.NUXT_PUBLIC_ZOOM_SDK_SECRET,
      zoomJWTKey: process.env.NUXT_PUBLIC_JWT_KEY,
      zoomJWTSecret: process.env.NUXT_PUBLIC_JWT_SECRET,
      zoomJWTToken: process.env.NUXT_PUBLIC_JWT_TOKEN,
      zoomUid: process.env.NUXT_PUBLIC_ZOOM_UID,
      zoomSdkKey2: process.env.NUXT_PUBLIC_ZOOM_SDK_KEY2,
      zoomSdkSecret2: process.env.NUXT_PUBLIC_ZOOM_SDK_SECRET2,
      zoomJWTKey2: process.env.NUXT_PUBLIC_JWT_KEY2,
      zoomJWTSecret2: process.env.NUXT_PUBLIC_JWT_SECRET2,
      zoomJWTToken2: process.env.NUXT_PUBLIC_JWT_TOKEN2,
      zoomUid2: process.env.NUXT_PUBLIC_ZOOM_UID2,

    },
  },
  // builder: 'webpack',
})
