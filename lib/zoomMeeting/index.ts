import ZoomMtgEmbeddedProxy from '@zoomus/websdk/embedded'
import { getMeetingOption } from '~/composables/constantAPI'
import { getToken } from '~/composables/server'

// setting up constant

const userEmail = ''
const registrantToken = ''
let ZoomMtgEmbedded: any
if ('default' in ZoomMtgEmbeddedProxy)
  ZoomMtgEmbedded = ZoomMtgEmbeddedProxy.default as any
else
  ZoomMtgEmbedded = ZoomMtgEmbeddedProxy as any

export const initZoom = async () => {
  const client = ZoomMtgEmbedded.createClient()
  const meetingSDKChatElement = document.getElementById('meetingSDKChatElement')
  const meetingSDKElement = document.getElementById('meetingSDKElement')
  if (meetingSDKElement) {
    await client.init({
      // debug: true,
      zoomAppRoot: meetingSDKElement,
      language: 'en-US',
      customize: {
        chat: {
          popper: {
            disableDraggable: false,
            anchorElement: meetingSDKChatElement,
            placement: 'top-start',
          },
        },
        video: {
          isResizable: true,
          popper: {
            disableDraggable: true,
          },
          viewSizes: {},
        },
        meetingInfo: ['topic', 'host', 'mn', 'pwd', 'telPwd', 'invite', 'participant', 'dc', 'enctype'],
        toolbar: {
          buttons: [],
        },
      },
    })
  }

  return client
}

export const startZoomCall = async (
  client: any,
  meetingNumber: string,
  role: 0 | 1,
  passWord: string,
  callIndex: number,
  user: string,
) => {
  const { sdkKey } = getMeetingOption(callIndex, ['sdkKey'])
  const signature = await getToken(callIndex, meetingNumber, role)
  return client.join({
    sdkKey,
    signature: signature.signature,
    meetingNumber,
    password: passWord,
    userName: user,
    userEmail,
    tk: registrantToken,
  })
}

export type StartZoomCall = typeof startZoomCall
