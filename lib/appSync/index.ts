import {
  ApolloClient, ApolloLink, HttpLink,
  InMemoryCache,
  gql,
} from '@apollo/client'
import * as Rx from 'rxjs'

import { v4 as uuidv4 } from 'uuid'
import _ from 'lodash'
import { createAuthLink } from 'aws-appsync-auth-link'
import { createSubscriptionHandshakeLink } from 'aws-appsync-subscription-link'
import type { Observable } from 'zen-observable-ts'
import type { ChatMessage } from '~/store/chat'

const url = 'https://ddgcpt7d7fggvlusp74d6e77vu.appsync-api.ap-northeast-1.amazonaws.com/graphql'
const region = 'ap-northeast-1'
const auth = {
  type: 'API_KEY' as const,
  apiKey: 'da2-ggmnj56gg5esdmgkr5y7tk5wym',
  // jwtToken: async () => token, // Required when you use Cognito UserPools OR OpenID Connect. token object is obtained previously
  // credentials: async () => credentials, // Required when you use IAM-based auth.
}
const zenToRx = <T>(zenObservable: Observable<T>): Rx.Observable<T> =>
  new Rx.Observable(
    observer => zenObservable.subscribe(observer))

const httpLink = new HttpLink({ uri: url })

const link = ApolloLink.from([
  createAuthLink({ url, region, auth }),
  createSubscriptionHandshakeLink({ url, region, auth }, httpLink),
])

const client = new ApolloClient({
  link,
  cache: new InMemoryCache(),
  defaultOptions: {
    watchQuery: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'ignore',
    },
    query: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'all',
    },
  },
  // headers: {
  //   'X-Api-Key': 'da2-dvqw7u4drbbknbkrypsgmoruvq' as string,
  // },

})
type Roles = 'Student' | 'Company0' | 'Company1'
// QA
export function onCreatePoll(callID: string) {
  const query = gql`
  subscription MySubscription($callID: String) {
  onCreatedPoll(callId: $callID) {
    id
    question
    released
    stop
    timeStamp
    callId
    Answer {
      answer
      id
      noOfAnswer
    }
  }
}
`
  return zenToRx(client
    .subscribe({
      query,
      variables: {
        callID,
      },
    }))
}
export function onUpdatePoll() {
  const query = gql`
  subscription MySubscription {
  onUpdatedPoll {
    id
    question
    released
    stop
    timeStamp
    callId
    Answer {
      answer
      id
      noOfAnswer
    }
  }
}
`
  return zenToRx(client
    .subscribe({
      query,
    }))
}
export function onCreateAnswer() {
  const query = gql`
  subscription MySubscription {
  onCreatedAnswer {
    answer
    id
    noOfAnswer
    pollId
  }
}
`
  return zenToRx(client
    .subscribe({
      query,
    }))
}
export function onUpdateAnwer() {
  const query = gql`
  subscription MySubscription {
  onUpdatedAnswer {
    answer
    id
    noOfAnswer
    pollId
  }
}
`
  return zenToRx(client
    .subscribe({
      query,
    }))
}
// QA
export function onCreateQA(callID: string) {
  const query = gql`
  subscription MySubscription($callID: String) {
  onCreatedQA(callId: $callID) {
    id
    question
    tick
    user {
      name
      id
    }
    timeStamp
    answerer {
      name
      id
    }
    answer
    callId
  }
}

`
  return zenToRx(client
    .subscribe({
      query,
      variables: {
        callID,
      },
    }))
}
export function onUpdateQA(callID: string) {
  const query = gql`
  subscription MySubscription($callID: String) {
  onMutatedQA(callId: $callID) {
    id
    question
    tick
    user {
      name
      id
    }
    timeStamp
    answerer {
      name
      id
    }
    answer
    callId
  }
}
`
  return zenToRx(client
    .subscribe({
      query,
      variables: {
        callID,
      },
    }))
}
// Call
export function onCreateorDeleteCall() {
  const query = gql`
  subscription MySubscription {
  onMutatedZoomAccount {
    id
    call {
      id
      expiration
      number
      record
      commentBan
      password
      pinnedCommentId
      createdBy {
        position
      }
    }
  }
}
`
  return zenToRx(client
    .subscribe({
      query,
    }))
}
export function onUpdateCall() {
  const query = gql`
  subscription MySubscription {
  onUpdatedCall{
    id
    expiration
    number
    record
    commentBan
    password
    pinnedCommentId
    createdBy {
      position
    }
  }
}
`
  return zenToRx(client
    .subscribe({
      query,
    }))
}
// Chat
export function onCreateComment(callID: String) {
  const query = gql`
  subscription MySubscription($callId: String) {
  onCreatedComment(callId: $callId) {
    id
    message
    timeStamp
    user {
      id
      name
    }
  }
}
`
  return zenToRx(client
    .subscribe({
      query,
      variables: {
        callID,
      },
    }))
}
export function onDeleteComment(callID: String) {
  const query = gql`
  subscription MySubscription( $callId: String) {
  onDeletedComment(callId: $callId) {
    id
  }
}
`
  return zenToRx(client
    .subscribe({
      query,
      variables: {
        callID,
      },
    }))
}
//
// Chat
/**
 *
 * @param message ChatMessage
 * @param callID number
 * @returns data
 */
export function addChat(message: ChatMessage, callID: String) {
  const query = gql`
    mutation MyMutation(
  $id: String!
  $callID: String!
  $message: String!
  $timeStamp: AWSDateTime!
  $sender: String!
) {
  createComment(
    data: {
      call: { connect: { id: $callID } }
      id: $id
      message: $message
      timeStamp: $timeStamp
      user: { connect: { id: $sender } }
    }
  ) {
    id
    message
    timeStamp
    user {
      id
      name
    }
  }
}`
  return client
    .mutate({
      mutation: query,
      variables: {
        ...message,
        id: uuidv4(),
        callID,
      },
    }).then(result => result.data.createComment)
}
// No need callID.
/**
 * @param id string
 * @returns data
 */
export function removeChat(id: string) {
  const query = gql`
    mutation MyMutation($id: String!){
      deleteComment(where: {id: $id}) {
        id
        message
        callId
      }
    }
  `
  return client
    .mutate({
      mutation: query,
      variables: {
        id,
      },
    }).then(result => result.data.deleteComment)
}

// Question
/**
 *
 * @param message QAType
 * @param callID number
 * @returns data
 */

export function addQA(message: string, sender: string, callId: string) {
  const query = gql`
    mutation MyMutation(
      $timeStamp: AWSDateTime!
      $id: String!
      $callId: String!
      $sender: String! 
      $message: String!
) {
  createQA(
    data: {
      id: $id
      question: $message
      tick: false
      timeStamp: $timeStamp
      call: { connect: { id: $callId } }
      user: { connect: { id: $sender } }
    }
  ) {
    id
    question
    tick
    user {
      name
      id
    }
    timeStamp
    answerer {
      name
      id
    }
    answer
    callId
  }
}`
  return client
    .mutate({
      mutation: query,
      variables: {
        message,
        sender,
        callId,
        id: uuidv4(),
        timeStamp: (new Date()).toISOString(),
      },
    }).then(result => result.data.createQA)
}

/**
 *
 * @param answer string
 * @param id ID QA | string
 * @param answerer Id answerer | string
 * @returns data
 */
export function answerQA(answer: string, id: string, answerer: string) {
  const query = gql`
    mutation MyMutation($answer: String!, $idQA: String!, $answerer: String!) {
      updateQA(where: {id:  $idQA}, data: {answer: $answer, answerer: {connect: {id: $answerer}}}) {
        id
        question
        tick
        user {
          name
          id
        }
        timeStamp
        answerer {
          name
          id
        }
        answer
        callId
      }
    }
  `
  return client
    .mutate({
      mutation: query,
      variables: {
        answer,
        idQA: id,
        answerer,
      },
    }).then(result => result.data.updateQA)
}
/**
 *
 * @param id ID QA string
 * @param answerer ID answerer string
 * @param tickQA boolean
 * @returns data
 */
export function tickQA(id: string, answerer: string) {
  const query = gql`
    mutation MyMutation($id: String!, $answerer: String!) {
      updateQA(where: {id:  $id}, data: { answerer: {connect: {id: $answerer}}, tick: true}) {
        id
        question
        tick
        user {
          name
          id
        }
        timeStamp
        answerer {
          name
          id
        }
        answer
        callId
      }
    }
  `
  return client
    .mutate({
      mutation: query,
      variables: {
        id,
        answerer,
      },
    }).then(result => result.data.updateQA)
}

/**
 *
 * @param name name user - string
 * @param position Roles : "Student" | "Company0" | "Company1"
 * @returns data
 */
export function createUser(name: string, position: Roles) {
  const query = gql`
  mutation MyMutation($name: String!, $id: String!, $position: Role!) {
    createUser(data: {name: $name, id: $id, position: $position}) {
      id
      name
      position
    }
  }
  `

  return client
    .mutate({
      mutation: query,
      variables: {
        name,
        id: uuidv4(),
        position,
      },
    }).then(result => result.data.createUser)
}
export function changeName(name: string, id: string) {
  const query = gql`
  mutation MyMutation(
  $name: String!
  $id: String!
) {
  updateUser(where: { id: $id }, data: { name: $name }) {
    name
  }
}
  `
  return client
    .mutate({
      mutation: query,
      variables: {
        name,
        id,
      },
    }).then(result => result.data.createUser)
}

export function getListQa(callId: string) {
  const query = gql`
  query MyQuery($callId: String!) {
  listQAs(where: {call: {is: {id: $callId}}}, orderBy: {timeStamp: ASC}) {
    id
    question
    tick
    user {
      name
      id
    }
    timeStamp
    answerer {
      name
      id
    }
    answer
  }
}`

  return client
    .mutate({
      mutation: query,
      variables: {
        callId,
      },
    }).then(result => result.data.listQAs)
}

// Poll
/**
 *
 * @param question string
 * @param callId string
 * @returns data
 */
export function newPoll(question: string, callId: string) {
  const query = gql`
    mutation MyMutation(
  $id: String!
  $callId: String! 
  $question: String! 
  $released: Boolean = false
  $stop: Boolean = false
  $timeStamp: AWSDateTime!
) {
  createPoll(
    data: {
      id: $id
      question: $question
      released: $released
      stop: $stop
      timeStamp: $timeStamp
      call: { connect: { id: $callId } }
    }
  ) {
    id
    question
    released
    stop
    callId
    timeStamp
    Answer {
      answer
      id
      noOfAnswer
    }
  }
}`

  return client
    .mutate({
      mutation: query,
      variables: {
        id: uuidv4(),
        callId,
        question,
        timeStamp: (new Date()).toISOString(),
      },
    }).then(result => result.data.createPoll,
    )
}

/**
 *
 * @param id string
 * @returns data
 */
export function deletePoll(id: string) {
  const query = gql`
  mutation MyMutation($id: String!) {
  updatePoll(
    where: { id: $id }
    data: { call: { disconnect: true } }
  ) {
    id
    question
    released
    stop
    timeStamp
    callId 
    Answer {
      answer
      id
      noOfAnswer
    }
  }
}`
  return client
    .mutate({
      mutation: query,
      variables: {
        id,
      },
    }).then(result => result.data.updatePoll,
    )
}
export function stopPoll(id: string) {
  const query = gql`
  mutation MyMutation($id: String) {
  updatePoll(where: {id: $id}, data: {stop: true}) {
    id
    question
    released
    stop
    timeStamp
    callId 
    Answer {
      answer
      id
      noOfAnswer
    }
  }
}
`
  return client
    .mutate({
      mutation: query,
      variables: {
        id,
      },
    }).then(result => result.data.updatePoll,
    )
}
/**
 *
 * @param id string
 * @returns data
 */
export function releasedPoll(id: string) {
  const query = gql`
  mutation MyMutation($id: String) {
  updatePoll(where: { id: $id }, data: { released: true }) {
    id
    question
    released
    stop
    timeStamp
    callId 
    Answer {
      answer
      id
      noOfAnswer
    }
  }
}`
  return client
    .mutate({
      mutation: query,
      variables: {
        id,
      },
    }).then(result => result.data.updatePoll,
    )
}
/**
 *
 * @param id string
 * @returns data
 */
export function votePoll(id: string) {
  const query = gql`
  mutation MyMutation($id: String) {
  updateAnswer(where: {id: $id}, operation: {noOfAnswer: {increment: 1}}) {
    answer
    id
    noOfAnswer
    pollId
  }
}`
  return client
    .mutate({
      mutation: query,
      variables: {
        id,
      },
    }).then(result => result.data.updateAnswer,
    )
}

/**
 *
 * @param answer string
 * @param pollId string
 * @returns data
 */
export async function createAns(answer: string, pollId: string) {
  const query = gql`
  mutation MyMutation(
  $id: String!
  $pollId: String!
  $noOfAnswer: Int = 0
  $answer: String = "nice"
) {
  createAnswer(
    data: {
      answer: $answer
      noOfAnswer: $noOfAnswer
      id: $id
      poll: { connect: { id: $pollId } }
    }
  ) {
    answer
    id
    noOfAnswer
    pollId
  }
}`
  return client
    .mutate({
      mutation: query,
      variables: {
        id: uuidv4(),
        answer,
        pollId,
      },
    }).then(result => result?.data.createAnswer)
}

/**
 *
 * @param callId string
 * @returns data
 */
export function getListPoll(callId: string) {
  const query = gql`
  query MyQuery($callId: String!) {
  listPolls(where: {call: {is: {id: $callId}}}, orderBy: {timeStamp: ASC}) {
    id
    question
    released
    stop
    timeStamp
    callId 
    Answer {
      answer
      id
      noOfAnswer
    }
  }
}`

  return client
    .mutate({
      mutation: query,
      variables: {
        callId,
      },
    }).then(result => result?.data.listPolls)
}

/**
 *
 * @param callId string
 * @returns data
 */
export function getListComments(callId: string) {
  const query = gql`
  query MyQuery($callId: String!) {
  listComments(where: {call: {is: {id: $callId}}}, orderBy: {timeStamp: DESC }) {
    id
    message
    timeStamp
    user {
      name
      id
    }
  }
}`

  return client
    .query({
      query,
      variables: {
        callId,
      },
    }).then(result => _.reverse(result.data.listComments))
}

/**
 *
 * @param value boolean
 * @param callId string
 * @returns data
 */
export function toggleBan(value: boolean, callId: string) {
  const query = gql`
  mutation MyMutation(
  $value: Boolean!
  $callId: String!
) {
  updateCall(where: { id: $callId }, data: { commentBan: $value }) {
    id
    expiration
    number
    record
    commentBan
    password
    pinnedCommentId
    createdBy {
      position
    }
  }
}`

  return client
    .query({
      query,
      variables: {
        callId,
        value,
      },
    }).then(result => result.data.updateCall)
}
/**
 *
 * @param commentId string
 * @param callId string
 * @returns data
 */
export function pinChat(commentId: string, callId: string) {
  const query = gql`
  mutation MyMutation(
  $commentId: String!
  $callId: String!
) {
  updateCall(where: { id: $callId }, data: { pinnedCommentId: $commentId }) {
    id
    expiration
    number
    record
    commentBan
    password
    pinnedCommentId
    createdBy {
      position
    }
  }
}`

  return client
    .query({
      query,
      variables: {
        callId,
        commentId,
      },
    }).then(result => result.data.updateCall)
}

/**
 *
 * @returns data
 */
export function getListCalls() {
  const query = gql`
  query MyQuery {
  listZoomAccounts {
    id
    call {
      id
      expiration
      number
      record
      commentBan
      password
      pinnedCommentId
      createdBy {
        position
      }
    }
  }
}`

  return client
    .query({
      query,
    }).then(result => result.data.listZoomAccounts)
}

/**
 *
 * @param number string
 * @param password string
 * @param userId string
 * @param accountId string
 * @returns data
 */
export function createCall(number: string, password: string, userId: string, accountId: string, date: string) {
  const query = gql`
  mutation MyMutation(
  $userId: String! 
  $expiration: AWSDateTime! 
  $id: String!
  $accountId: String! 
  $number: String! 
  $password: String!
) {
  updateZoomAccount(
    where: { id: $accountId }
    data: {
      call: {
        create: {
          commentBan: false
          expiration: $expiration
          id: $id
          number: $number
          password: $password
          createdBy: { connect: { id: $userId } }
        }
      }
    }
  ) {
    id
    call {
      id
      expiration
      number
      record
      commentBan
      password
      pinnedCommentId
      createdBy {
        position
      }
    }
  }
}`
  return client.mutate({
    mutation: query,
    variables: {
      id: uuidv4(),
      expiration: date || (new Date()).toISOString(),
      userId,
      accountId,
      number,
      password,
    },
  }).then(result => result.data.createCall)
}

/**
 *
 * @param id string
 * @returns data
 */
export function removeCall(id: string) {
  const query = gql`
  mutation MyMutation($id: String) {
  updateZoomAccount(where: { id: $id }, data: { call: { disconnect: true } }) {
    id
  }
}`

  return client
    .mutate({
      mutation: query,
      variables: {
        id,
      },
    }).then(result => result.data.getUser)
}

export function startCall(id: string) {
  const query = gql`
  mutation MyMutation($id: String, $expiration: AWSDateTime ) {
  updateCall(where: {id: $id}, data: {expiration: $expiration}) {
    id
    expiration
    number
    record
    commentBan
    password
    pinnedCommentId
    createdBy {
      position
    }
  }
}`

  return client
    .mutate({
      mutation: query,
      variables: {
        id,
        expiration: (new Date()).toISOString(),
      },
    }).then(result => result.data.getUser)
}

export function toggleRecord(record: boolean, id: string) {
  const query = gql`
  mutation MyMutation($id: String, $record: Boolean = false) {
  updateCall(where: {id: $id}, data: {record: $record}) {
    id
    expiration
    number
    record
    commentBan
    password
    pinnedCommentId
    createdBy {
      position
    }
  }
}
`
  return client
    .mutate({
      mutation: query,
      variables: {
        id,
        record,
      },
    }).then(result => result.data.updateCall)
}

/**
 *
 * @param id string
 * @returns data
 */
export function getUser(id: string) {
  const query = gql`
  query MyQuery($id: String!) {
    getUser(where: {id: $id}) {
    name
    position
    }
  }`

  return client
    .query({
      query,
      variables: {
        id,
      },
    }).then(result => result.data.getUser)
}

