FROM node:18-alpine

RUN mkdir -p /app

COPY .output/ /app

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

EXPOSE 3000/tcp

ENTRYPOINT ["node", "/app/server/index.mjs"]