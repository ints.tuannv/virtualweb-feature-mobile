// import { ZoomMtg } from '@zoomus/websdk'
import { useZoomClientStore } from '~/store/zoomClient'

export function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

export function getElementByXpath(path: string) {
  return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue
}

export function audioComfirm() {
  const button = document.querySelector('.join-audio-by-voip__join-btn') as HTMLButtonElement

  if (button) {
    if (window.confirm('コンピュータオーディオに参加する')) {
      button.click()
      return
    }
  }
  const audioButton = document.querySelector('[aria-label="join audio"]') as HTMLButtonElement
  if (audioButton)
    audioButton.click()
}

export async function zoomUiModify() {
  const zoomClient = useZoomClientStore()
  if (zoomClient.uiChanging)
    return
  zoomClient.uiChanging = true
  await sleep(500)
  const button = document.querySelector('button[aria-label*="participant"]') as HTMLButtonElement
  if (button) {
    button.click()
    await sleep(300)

    const dropdown = document.querySelector('#participantSectionMenu')
    if (dropdown) {
      await sleep(300)
      const popOut = document.querySelector('.participants-header__pop-out-icon') as HTMLButtonElement
      if (popOut)
        popOut.click()
    }
  }

  await sleep(400)
  audioComfirm()
  await sleep(200)
  zoomClient.uiChanging = false

  ZoomMtg.getAttendeeslist({
    success: (i: any) => zoomClient.participantsList = i.result.attendeesList,
  })
}

export async function shareScreen() {
  await sleep(100)
  const button = document.querySelector('button[aria-label="more security options"]') as HTMLButtonElement

  if (button) {
    button.click()
    await sleep(300)
    const btnShare = document.querySelector('[aria-label="allow participant to 画面を共有 unselect"]') as HTMLButtonElement
    if (btnShare)
      btnShare.click()
  }
}
