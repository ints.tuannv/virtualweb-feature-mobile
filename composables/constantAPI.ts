type Key = 'sdkKey' | 'sdkSecret' | 'jwtToken' | 'uid'
type result = {
  [key in Key]?: string
}

export const getMeetingOption = (id: number, keysArr: Key[]) => {
  const config = useRuntimeConfig()
  const meetingOption = [
    {
      sdkKey: config.public.zoomSdkKey,
      sdkSecret: config.public.zoomSdkSecret,
      jwtToken: config.public.zoomJWTToken,
      uid: config.public.zoomUid,
    },
    {
      sdkKey: config.public.zoomSdkKey2,
      sdkSecret: config.public.zoomSdkSecret2,
      jwtToken: config.public.zoomJWTToken2,
      uid: config.public.zoomUid2,
    },
  ]

  const optionChoosed = meetingOption[id]

  return keysArr.reduce((acc, cur) => ({
    ...acc,
    [cur]: optionChoosed[cur],
  }), {} as result)
}

export const AccountCode = {
  Moderator: 'm',
  Company: 'c',
  Student: 's',
}
export const AccountLevel = {
  Moderator: 2,
  Company: 1,
  Student: 0,
}
export type AccountRole = keyof typeof AccountCode
