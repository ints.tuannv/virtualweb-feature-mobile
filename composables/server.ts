// This function should be written in a server, I will put it in FastApi after demo
import axios from 'axios'
import { KJUR } from 'jsrsasign'

export async function createMeeting(callIndex: number) {
  const {
    jwtToken,
    uid,
  } = getMeetingOption(callIndex, ['jwtToken', 'uid'])
  const header = {
    'content-type': 'application/json',
    'Authorization': `Bearer ${jwtToken}`,
  }
  const body = {
    topic: 'Zoom meeting title',
    type: 2,
    agenda: 'test',

    recurrence: {
      type: 1,
      repeat_interval: 1,
    },
    settings: {
      host_video: 'true',
      participant_video: 'False',
      join_before_host: 'False',
      mute_upon_entry: 'true',
      watermark: 'true',
      audio: 'voip',
      auto_recording: 'cloud',
    },
  }

  const idData = await axios({
    method: 'get',
    url: `https://api.zoom.us/v2/users/${uid}/meetings?type=live`,
    headers: header,
    data: body,
  })
  const id = idData.data.meetings?.[0]?.id
  if (id) {
    await axios({
      method: 'put',
      url: `https://api.zoom.us/v2/meetings/${id}/status`,
      headers: header,
      data: { action: 'end' },
    })
  }

  const response = await axios({
    method: 'post',
    url: `https://api.zoom.us/v2/users/${uid}/meetings`,
    headers: header,
    data: body,
  })
  return {
    meeting: {
      meetingNumber: response.data.id,
      passWord: response.data.password,
    },
  }
}

export async function getToken(callIndex: number, meetingNumber: string, role: 0 | 1) {
  const {
    sdkKey,
    sdkSecret,
  } = getMeetingOption(callIndex, ['sdkKey', 'sdkSecret'])
  const iat = Math.round(new Date().getTime() / 1000) - 30
  const exp = iat + 60 * 60 * 1

  const oHeader = {
    alg: 'HS256',
    typ: 'JWT',
  }
  const oPayload = {
    sdkKey,
    mn: meetingNumber,
    role,
    iat,
    exp,
    appKey: sdkKey,
    tokenExp: exp,
  }

  const sHeader = JSON.stringify(oHeader)
  const sPayload = JSON.stringify(oPayload)
  const signature = KJUR.jws.JWS.sign('HS256', sHeader, sPayload, sdkSecret)
  return { signature }
}
