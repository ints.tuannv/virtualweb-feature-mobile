export type Role = 'Student' | 'Company0' | 'Company1'

export interface DataCall {
  id: string
  call: {
    id: string
    expiration: string
    number: string
    password: string
    pinnedCommentId?: string
    commentBan: boolean
    createdBy: {
      position: Role
    }
    record: boolean
  }
}

export type Call = Omit<DataCall['call'], 'createdBy'> & { userID: string }

export interface QuesData {
  questionId: string
  answer: string
}

export interface User {

}
export type RemoveEvent = () => void
