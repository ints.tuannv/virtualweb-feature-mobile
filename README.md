# Overview
App có 3 phần database, api, front end

Database: database ở trên rds dc config bởi luvina

APi: gồm có appsync và lambda sẽ được autodeploy băng aws cdk

Front end viết băng vue3

## Setup và deploy api
Code sẽ dược generate rồi sẽ deploy lên trên amazon, code chỉ chaỵ dc trên môi trường unix nên phải cái 
ubuntu, _ cài docker để generate ra code (bước tiên)
### Bước 1 : cài docker trên window
cai docker desktop(window)

Docker sẽ cần để 

### Bước 2: cài ubuntu trên window (project không chạy dc trên window)

Cài ubuntu 20 tự động nếu cài wsl2 theo cách dưới của em(nguồn:  https://learn.microsoft.com/en-us/windows/wsl/install )

chạy trong power shell(admin) wsl --install

khởi động lại máy nó tự động cài đến màn hình hiển thị tạo user + pass là done nhé

Cài node 16 trên wsl2(nguồn:  https://computingforgeeks.com/how-to-install-node-js-on-ubuntu-debian/ )

copy vào console curl -sL  https://deb.nodesource.com/setup_16.x  | sudo bash -

kiểm tra cat /etc/apt/sources.list.d/nodesource.list

=> nếu có 2 dòng dưới thì okdeb  https://deb.nodesource.com/node_16.x  focal maindeb-src  https://deb.nodesource.com/node_16.x  focal main

cài đặt  sudo apt -y install nodejs

check node  -v


### Bước 3 cài aws phục vụ cho việc deploy
chúng ta sẽ deploy trên cdk của amazon

cài AWS CLI(https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)

gõ lệnh trong ubuntu :aws configure

nhập Aws access key và aws secret access key

va region name

### Bước 4 cái package manager
npm i -g yarn aws-cdk pnpm

### Bước 5 tạo folder
go to root folder of ubuntu (type \\wsl$ at folder directory)

mkdir prisma_appsync

cd prisma_appsync


### Bước 6 tạo project
npx create-prisma-appsync-app@1.0.0-rc.1

chọn current div and yes to all

### Bước 7 hoàn thành
copy file schema.prisma to prisma folder
yarn deploy

## Front end
Code front end có thể chạy trên bất kỳ môi trương nào chỉ cần nodejs 16
# Bước 1 
clone project về file zip hoặc gitlab
# Bước 2 
npm i -g pnpm
# Bước 3
cd vào project chạy
pnpm i dể ínstall 
# Bước 4 
chạy pnpm run dev
